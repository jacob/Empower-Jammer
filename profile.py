#!/usr/bin/env python
import geni.portal as portal
import geni.rspec.pg as rspec
import geni.rspec.igext as IG
import geni.rspec.emulab.pnext as PN
import geni.urn as URN

tourDescription = """
# 5G-Empower Controlled RF: Spectrum DOS Attack
Use this profile to intantiate the 5G-Empower software defined networking platform
within a controlled RF environment and end-to-end LTE network that features
jammer and sensor nodes for launching and detecting a spectrum attack
(wired connections between UE and eNB). The UE can be srsLTE-based or a Nexus 5.

If you elect to use a Nexus 5, these nodes will be deployed:
* Nexus 5 (`rue1`)
* Generic Compute Node w/ ADB image (`adbnode`)
* Generic Compute Node w/ Empower image (`controller`)
* Generic Compute Node w/ srsLTE epc image (`epc`)
* Intel NUC5300/B210 w/ srsLTE eNB (`enb1`)
* Intel NUC5300/B210 w/ gnuRadio (`jammer`)
* Intel NUC5300/B210 w/ gnuRadio (`sensor`)

If instead you choose to use an srsLTE UE, these will be deployed:
* Intel NUC5300/B210 w/ srsLTE (`rue1`) or
* Intel NUC5300/B210 w/ srsLTE eNB/EPC (`enb1`)
"""
tourInstructions = """
### NOTE
Ensure that startup has finished in all nodes
Use ""-Y" option in ssh to ensure full functionality

### Start EPC
After your experiment becomes ready, login to `epc` via `ssh` and run the command below:
```
# start srsepc
sudo srsepc /etc/srslte/epc.conf
```
This will start the srsEPC

### Start eNb
After your experiment becomes ready, login to `enb1` via `ssh` and run the command below:
```
# start srsenb
cd /opt/srsLTE-20.04/build/srsenb/src && sudo ./srsenb enb.conf
```
This will start the srseNB. After you've associated
a UE with this eNB, you can use the third tab to run tests with `ping` or
`iperf`.
NOTE: If the radio is not detected, power cycle the eNB node.

### Start 5G-Empower Controller
Login to `controller` and run:
```
# start empower controller
cd /opt/empower-runtime/ && sudo ./empower-runtime.py
```
Add the eNb to the controller and set up the project
```
# In a new contrller terminal
cd /opt/empower-runtime/ && ./empower-ctl.py add-vbs -a 00:00:00:00:01:9b -d eNb1
# password <root>
./empower-ctl.py create-project -c 99898 -d test -o foo
# password <root> returns your project_id
```

To start a basic UE measurement app
Replace with your project ID from above and with the imsi for your ue
\nue1: 998981234560300
\nue2: 998981234560301
\nue3: 998981234560302
\nue4: 998981234560303
```
# in a new controller terminal
sudo python3 empower-ctl.py load-app --project_id=<Your Project ID> --name=empower.apps.uemeasurements.uemeasurements --imsi=<UE for reports> --meas_id=1 --interval=MS2048 --amount="INFINITY"
# the password is <root>
```
After starting the app you should see UE measurement reports in the output of the eNb node

For information about configuring the controller or running apps visit:

https://github.com/5g-empower/5g-empower.github.io/wiki/Configuring-up-the-Controller

### Nexus 5
If you've deployed a Nexus 5, you should see it sync with the eNB eventually and
obtain an IP address. Login to `adbnode` in order to access `rue1` via `adb`:
```
# on `adbnode`
pnadb -a
adb shell
```
Once you have an `adb` shell to `rue1`, you can use `ping` to test the
connection, e.g.,
```
# in adb shell connected to rue1
# ping SGi IP
ping 172.16.0.1
```
If the Nexus 5 fails to sync with the eNB, try rebooting it via the `adb` shell.
After reboot, you'll have to repeat the `pnadb -a` and `adb shell` commands on
`adbnode` to reestablish a connection to the Nexus 5.

### Jammer
Login to the jammer and run:
```
uhd_siggen_gui -f 2132.5M -g 80 --amplitude 0.85 --gaussian
```
With default settings there should be about 20% packet loss on the link\n
If the gain is increased to 82 there should be around 60%
Try running
```
sudo apt install python3-gi gobject-introspection gir1.2-gtk-3.0
```
If throws an error saying GTK is not available

### Sensor
Login to the sensor and run:
```
uhd_fft -s 50M -f CENTER_FREQUENCY
```
Replace CENTER_FREQUENCY with
1732.5M  for UL
2132.5M  for DL


### srsLTE UE (Untested)
If you've deployed an srsLTE UE, login to `rue1` and do:
```
/local/repository/bin/start.sh
```
This will start a `tmux` session with two panes: one running srsue and the other
holding a spare terminal for running tests with `ping` and `iperf`.
If you are not familiar with `tmux`, it's a terminal multiplexer that
has some similarities to screen. Here's a [tmux cheat
sheet](https://tmuxcheatsheet.com), but `ctrl-b o` (move to other pane) and
`ctrl-b x` (kill pane), should get you pretty far. `ctrl-b d` will detach you
from the `tmux` session and leave it running in the background. You can reattach
with `tmux attach`. If you'd like to run `srsue` manually, do:
```
sudo srsue /etc/srslte/ue.conf
```

"""
#Experimental Images
#Controller image: urn:publicid:IDN+emulab.net+image+reu2020:Empower-Controller
#nextEPC eNb image: urn:publicid:IDN+emulab.net+image+reu2020:Empower-eNb-epc
#no next eNB image: urn:publicid:IDN+emulab.net+image+reu2020:srsLTE_Empower.enb1


class GLOBALS(object):
    NUC_HWTYPE = "nuc5300"
    EPC_HWTYPE = "d840" #srsEPC has issues running on some other hardware types
    COTS_UE_HWTYPE = "nexus5"
    UBUNTU_1804_IMG = "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU18-64-STD"
    CONTROL_IMG = "urn:publicid:IDN+emulab.net+image+emulab-ops:UBUNTU20-64-STD" #Used Ubuntu20 for better python support
    SRSLTE_IMG = "urn:publicid:IDN+emulab.net+image+PowderProfiles:U18LL-SRSLTE:1" #Low-latency image
    ENB_IMG = "urn:publicid:IDN+emulab.net+image+reu2020:Empower-Jammer.enb1"
    COTS_UE_IMG = URN.Image(PN.PNDEFS.PNET_AM, "PhantomNet:ANDROID444-STD")
    ADB_IMG = URN.Image(PN.PNDEFS.PNET_AM, "PhantomNet:UBUNTU14-64-PNTOOLS")
    JAM_IMG = "urn:publicid:IDN+emulab.net+image+reu2020:Empower-Jammer.jammer"


pc = portal.Context()
#Allow the user to forgo an Empower Controller Node
pc.defineParameter("controller_num", "Controller Quanitity", portal.ParameterType.STRING, "0",
                   [("0", "0"), ("1", "1")],
                   longDescription="Number of controller nodes to deploy. Defaults to 0; Set to 1 if using Empower")
#Allow the user specify the number of eNb
pc.defineParameter("eNb_num", "NUC Quantity", portal.ParameterType.STRING, "3",
                   [("1", "1"), ("2", "2"), ("3", "3")],
                   longDescription="Number of nucs with SDRs to deploy")
#Allow the user specify the number of eNb
pc.defineParameter("EPC_num", "EPC Quantity", portal.ParameterType.STRING, "0",
                   [("0", "0"), ("1", "1")],
                   longDescription="Number of dedicated EPCs to deploy")
#Allow the user to use a Nexus 5 or srsUE
pc.defineParameter("ue_type", "UE Type", portal.ParameterType.STRING, "srsue",
                   [("srsue", "srsLTE UE (B210)"), ("nexus5", "COTS UE (Nexus 5)")],
                   longDescription="Type of UE to deploy.")
#Allow the user to specify a specific UE
pc.defineParameter("FIXED_UE", "Bind to a specific UE", portal.ParameterType.STRING, "nuc4",
                   [("ue1", "ue1 (Nexus5)"), ("ue2", "ue2 (Nexus5)"), ("ue3", "ue3 (Nexus5)"), ("ue4", "ue4 (Nexus5)"), ("nuc1", "nuc1 (srsUE)"), ("nuc2", "nuc2 (srsUE)"), ("nuc3", "nuc3 (srsUE)"), ("nuc4", "nuc4 (srsUE)"), ("None", "Default")],
                   longDescription="Input the name of a POWDER controlled RF UE node to allocate (e.g., 'ue1').  Leave blank to let the mapping algorithm choose.")
#Allow the user to specify a specific eNb
pc.defineParameter("FIXED_ENB", "Bind to a specific eNb1", portal.ParameterType.STRING, "nuc3",
                   [("nuc7", "nuc7"), ("nuc8", "nuc8"), ("nuc9", "nuc9"), ("nuc10", "nuc10"), ("nuc1", "nuc1"), ("nuc2", "nuc2"), ("nuc3", "nuc3"), ("nuc4", "nuc4"), ("None", "Default")],
                   longDescription="Input the name of a POWDER controlled RF device to allocate (e.g., 'nuc1').  Leave blank to let the mapping algorithm choose.  If you bind both UE and eNodeB devices, mapping will fail unless there is path between them via the attenuator matrix.")
#Allow the user to specify a specific eNb
pc.defineParameter("FIXED_ENB2", "Bind to a specific jammer", portal.ParameterType.STRING, "nuc1",
                   [("nuc7", "nuc7"), ("nuc8", "nuc8"), ("nuc9", "nuc9"), ("nuc10", "nuc10"), ("nuc1", "nuc1"), ("nuc2", "nuc2"), ("nuc3", "nuc3"), ("nuc4", "nuc4"), ("None", "Default")],
                   longDescription="Input the name of a POWDER controlled RF device to allocate (e.g., 'nuc1').  Leave blank to let the mapping algorithm choose.  If you bind both UE and eNodeB devices, mapping will fail unless there is path between them via the attenuator matrix.")
#Allow the user to specify a specific eNb
pc.defineParameter("FIXED_SEN", "Bind to a specific Sensor", portal.ParameterType.STRING, "nuc2",
                   [("nuc7", "nuc7"), ("nuc8", "nuc8"), ("nuc9", "nuc9"), ("nuc10", "nuc10"), ("nuc1", "nuc1"), ("nuc2", "nuc2"), ("nuc3", "nuc3"), ("nuc4", "nuc4"), ("None", "Default")],
                   longDescription="Input the name of a POWDER controlled RF device to allocate (e.g., 'nuc1').  Leave blank to let the mapping algorithm choose.  If you bind both UE and eNodeB devices, mapping will fail unless there is path between them via the attenuator matrix.")
#Specify Hardware for EPC
pc.defineParameter("epcHw", "EPC hardware type", portal.ParameterType.STRING, "d430",
                   [("d430", "d430"), ("d840", "d840")],
                   longDescription="Type of hardware to use for EPC. Stability issues are present on other HW types")

params = pc.bindParameters()

#Check for exceptions and warnings
pc.verifyParameters()

#Create model of the RSpec
request = pc.makeRequestRSpec()

#Create the RF link
rflink = request.RFLink("rflink")
rflink3 = request.RFLink("rflink3")
rflink9 = request.RFLink("rflink9")

#Create the LAN
epclink = request.Link("s1-lan")

#Add a Controller Node
if params.controller_num == "1":
    controller = request.RawPC("controller")
    controller.disk_image = GLOBALS.CONTROL_IMG
    ifaceC = controller.addInterface("ifC")
    ifaceC.addAddress(rspec.IPv4Address("10.10.2.2", "255.255.255.0")) #Assign IP to the interface
    epclink.addInterface(ifaceC) #Add to the lan
    controller.addService(rspec.Execute(shell="bash", command="/local/repository/bin/Controller-Script.sh"))

#Add an EPC Node
if (params.EPC_num == 1):
    epc = request.RawPC("epc")
    epc.disk_image = GLOBALS.SRSLTE_IMG
    epc.hardware_type = params.epcHw
    ifaceE1 = epc.addInterface("ifE1")
    ifaceE1.addAddress(rspec.IPv4Address("10.10.2.1", "255.255.255.0")) #Assign IP to the interface
    epclink.addInterface(ifaceE1) #Add to the lan
    ifaceE2 = epc.addInterface("ifE2")
    ifaceE2.addAddress(rspec.IPv4Address("10.10.2.3", "255.255.255.0")) #Assign IP to the interface
    epclink.addInterface(ifaceE2) #Add to the lan
    if params.controller_num == 0:#If there is not a separate controller add its ip to the EPC
        ifaceC2 = epc.addInterface("ifC2")
        ifaceC2.addAddress(rspec.IPv4Address("10.10.2.2", "255.255.255.0")) #Assign IP to the interface
        epclink.addInterface(ifaceC2) #Add to the lan
    epc.addService(rspec.Execute(shell="bash", command="/local/repository/bin/update-config-files.sh"))
    epc.addService(rspec.Execute(shell="bash", command="/local/repository/bin/tune-cpu.sh"))
    epc.addService(rspec.Execute(shell="bash", command="/local/repository/bin/add-nat-and-ip-forwarding.sh"))
    epc.addService(rspec.Execute(shell="bash", command="/local/repository/bin/pythonSetup.sh"))



# Add a UE node
if params.ue_type == "nexus5":
    adbnode = request.RawPC("adbnode")
    adbnode.disk_image = GLOBALS.ADB_IMG
    rue1 = request.UE("rue1")
    if params.FIXED_UE != "None": #Use a specific UE
        rue1.component_id = params.FIXED_UE
    rue1.hardware_type = GLOBALS.COTS_UE_HWTYPE
    rue1.disk_image = GLOBALS.COTS_UE_IMG
    rue1.adb_target = "adbnode"
elif params.ue_type == "srsue":
    rue1 = request.RawPC("rue1")
    if params.FIXED_UE != "None": #Use a specific UE
        rue1.component_id = params.FIXED_UE
    rue1.hardware_type = GLOBALS.NUC_HWTYPE
    rue1.disk_image = GLOBALS.SRSLTE_IMG
    ifaceUE = rue1.addInterface("ifUE")
    ifaceUE.addAddress(rspec.IPv4Address("10.10.2.10", "255.255.255.0")) #Assign IP to the interface
    epclink.addInterface(ifaceUE) #Add to the lan
    rue1.addService(rspec.Execute(shell="bash", command="/local/repository/bin/update-config-files.sh"))
    rue1.addService(rspec.Execute(shell="bash", command="/local/repository/tune-cpu.sh"))
    rue1.addService(rspec.Execute(shell="bash", command="/local/repository/bin/pythonSetup.sh"))
rue1.Desire("rf-controlled", 1)


#Add a NUC eNB node
enb1 = request.RawPC("enb1")
if params.FIXED_ENB != "None":
        enb1.component_id = params.FIXED_ENB
enb1.hardware_type = GLOBALS.NUC_HWTYPE
enb1.disk_image = GLOBALS.ENB_IMG
enb1.Desire("rf-controlled", 1)
enb1_rue1_rf = enb1.addInterface("rue1_rf") #Create rf interface to UE
rue1_enb1_rf = rue1.addInterface("enb1_rf") #Create rf interface from UE
rflink.addInterface(enb1_rue1_rf)
rflink.addInterface(rue1_enb1_rf)
iface3 = enb1.addInterface("eth1")
iface3.addAddress(rspec.IPv4Address("10.10.2.4", "255.255.255.0")) #Assign IP to the interface
epclink.addInterface(iface3) #Add to the lan
enb1.addService(rspec.Execute(shell="bash", command="/local/repository/bin/update-config-files.sh"))
enb1.addService(rspec.Execute(shell="bash", command="/local/repository/bin/tune-cpu.sh"))
enb1.addService(rspec.Execute(shell="bash", command="/local/repository/bin/add-nat-and-ip-forwarding.sh"))
enb1.addService(rspec.Execute(shell="bash", command="/local/repository/bin/pythonSetup.sh"))
# enb1.addService(rspec.Execute(shell="bash", command="/local/repository/bin/install_eNB.sh"))

#Add a NUC Sensor node if requested
if params.eNb_num == "2" or params.eNb_num == "3":
    sensor = request.RawPC("sensor")
    if params.FIXED_SEN != "None":
        sensor.component_id = params.FIXED_SEN
    sensor.hardware_type = GLOBALS.NUC_HWTYPE
    sensor.disk_image = GLOBALS.JAM_IMG
    sensor.Desire("rf-controlled", 1)
    sensor_enb1_rf = sensor.addInterface("enb1_rf") #Create rf interface to UE
    enb1_sensor_rf = enb1.addInterface("sensor_rf") #Create rf interface from UE
    rflink3.addInterface(sensor_enb1_rf)
    rflink3.addInterface(enb1_sensor_rf)
    iface5 = sensor.addInterface("eth1")
    iface5.addAddress(rspec.IPv4Address("10.10.2.5", "255.255.255.0")) #Assign IP to the interface
    epclink.addInterface(iface5) #Add to the lan
    sensor.addService(rspec.Execute(shell="bash", command="/local/repository/bin/update-config-files.sh"))
    sensor.addService(rspec.Execute(shell="bash", command="/local/repository/bin/tune-cpu.sh"))
    sensor.addService(rspec.Execute(shell="bash", command="/local/repository/bin/pythonSetup.sh"))

#If they request a second eNb add it
if params.eNb_num == "3":
    rflink2 = request.RFLink("rflink2")
    enb2 = request.RawPC("jammer")
    if params.FIXED_ENB2 != "None":
            enb2.component_id = params.FIXED_ENB2
    enb2.hardware_type = GLOBALS.NUC_HWTYPE
    enb2.disk_image = GLOBALS.JAM_IMG
    enb2.Desire("rf-controlled", 1)
    enb2_rue1_rf = enb2.addInterface("rue1_rf") #Create rf interface to UE
    rue1_enb2_rf = rue1.addInterface("enb2_rf") #Create rf interface from UE
    rflink2.addInterface(enb2_rue1_rf)
    rflink2.addInterface(rue1_enb2_rf)
    sensor_jam_rf = sensor.addInterface("enb3_rf") #Create rf interface to UE
    jam_sensor_rf = enb2.addInterface("sensor_rf") #Create rf interface from UE
    rflink9.addInterface(sensor_jam_rf)
    rflink9.addInterface(jam_sensor_rf)
    iface4 = enb2.addInterface("eth1")
    iface4.addAddress(rspec.IPv4Address("10.10.2.6", "255.255.255.0")) #Assign IP to the interface
    epclink.addInterface(iface4) #Add to the lan
    enb2.addService(rspec.Execute(shell="bash", command="/local/repository/bin/update-config-files.sh"))
    enb2.addService(rspec.Execute(shell="bash", command="/local/repository/bin/tune-cpu.sh"))
    enb2.addService(rspec.Execute(shell="bash", command="/local/repository/bin/add-nat-and-ip-forwarding.sh"))
    enb2.addService(rspec.Execute(shell="bash", command="/local/repository/bin/pythonSetup.sh"))

tour = IG.Tour()
tour.Description(IG.Tour.MARKDOWN, tourDescription)
tour.Instructions(IG.Tour.MARKDOWN, tourInstructions)
request.addTour(tour)

pc.printRequestRSpec(request)
