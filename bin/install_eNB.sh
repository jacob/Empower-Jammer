
#!/bin/bash
echo "installing the 5g Empower Agent and srsLTE..."
sudo apt-get update
sudo apt-get -y install cmake git libfftw3-dev libmbedtls-dev \
                     libboost-program-options-dev libconfig++-dev \
                     libsctp-dev libuhd-dev
cd //
cd opt
sudo git clone https://github.com/5g-empower/empower-enb-agent
cd empower-enb-agent
sudo cmake -DCMAKE_BUILD_TYPE=Release .
sudo make
sudo make install
cd ..
sudo git clone https://github.com/5g-empower/srsLTE-20.04
cd srsLTE-20.04
sudo mkdir build
cd build
sudo cmake ../
sudo make
cd ..
sudo cp srsenb/drb.conf.example build/srsenb/src/drb.conf
sudo cp /local/repository/etc/Configs/enb.conf build/srsenb/src/enb.conf
sudo cp srsenb/rr.conf.example build/srsenb/src/rr.conf
sudo cp srsenb/sib.conf.example build/srsenb/src/sib.conf
cd //
cd /usr/bin
echo "Installation has completed."
#etc
