#Import into the namespace
from socket import *
from subprocess import Popen, PIPE, STDOUT
#Import modules
import base64
import os
import sys
import time
import threading
import hashlib
import requests
import optparse
import iperf3
import signal

def startENB():
    enb = Popen(['srsenb', '/etc/srslte/enb.conf'], stdout=PIPE, stdin=PIPE, stderr=PIPE)
    print("Started enb")
    return enb

def startEPC():
    epc = Popen(['srsepc', '/etc/srslte/epc.conf'], stdout=PIPE, stdin=PIPE, stderr=PIPE)
    print("Started epc")
    return epc

def stopENB(enb):
    enb.send_signal(signal.SIGINT)
    print("Stopped enb")

def stopEPC(epc):
    epc.send_signal(signal.SIGINT)
    print("Stopped epc")

def startLTE(sockUE):
    epc = startEPC()
    time.sleep(10)
    enb = startENB()
    time.sleep(10)
    sendRequest("RUE", "START", sockUE)
    receive(sockUE)
    return (epc, enb)

def stopLTE(sockUE, epc, enb):
    stopEPC(epc)
    stopENB(enb)
    sendRequest("STP", "STOP", sockUE)
    receive(sockUE)
    return

def sendRequest(method, data, conn):
    """Sends a request to on the given socket"""

    req = method + '%' + data + '%ITS\r\n\r\n'

    conn.sendall(req.encode())
#END sendRequest

def send(method, data, conn):
    """Sends a request to on the given socket"""

    req = method + "%" + data + "%ITS\r\n\r\n"

    conn.sendall(req.encode())

    print ("\nSent:\n", req)
#END sendRequest

def iperfRun(freq):
    """Run an iperf3 client that connects to the host"""

    client = iperf3.Client()
    client.duration = 10
    client.server_hostname = '172.16.0.2'
    client.port = 5201
    result = client.run()

    print ("Received result", result.sent_Mbps, result.received_Mbps, result.retransmits)
    file.write(str(freq) + "," + str(result.sent_Mbps) + "," + str(result.received_Mbps) + "," + str(result.retransmits) + "\n")
#END iperfRun

def receive(sock):
    """Ensures that the whole response is read from the socket"""

    #Start with an empty response
    response = ""
    moredata = True

    #Loop until the everything is received
    while moredata:
        data = sock.recv(1024)
        #print "Received response"

        #Add each new chunk to the response
        response = response + data.decode()
        #print response
        #Catch the end of the header
        if(r'\r\n\r\n' in response or "\r\n\r\n" in response or '%ITS' in response):
            moredata = False

        if len(data) == 0:
            return "CSE"

    if verbose:
        print ("\nReceived: " + response)

    return response
#END receiveAll

def handleJammer(connSock, sockUE):
    """Handles a jammer over its lifetime"""
    print("Handling Jammer will wait for UE")

    time.sleep(50)
    sendRequest('IP3', 'START', sockUE)

    iperfRun(0)#Get a control test
    iperfRun(0)#Get a control test
    iperfRun(0)#Get a control test


    for f in range(freqStart, 2152500, 500):
        #epc, enb = startLTE(sockUE) Start srsLTE by hand
        time.sleep(40)
        #sendRequest('IP3', 'START', sockUE)
        sendRequest('JAM', (str(f)+'e3'), connSock)
        time.sleep(5)

        while True:

            #Get the request from the socket
            data = receive(connSock)

            #Break up the request and parse the URL
            decoded = data
            request = decoded.split('%')

            #print "\nReceived: ", decoded

            #If the client wants my public key
            if("SID" in request[0]):
                print("Identifier Received")

            #If the client wants my public key
            if("IP3" in request[0]):
                print("Reguest for iPerf trial")
                iperfRun(f)

            #If the the client returned a secure message
            elif("STR" in request[0]):
                if("STARTED" in request[1]):
                    print("Jammer Started")
                    iperfRun(f)
                    sendRequest("STP", "STOP", connSock)
                if("STOPPED" in request[1]):
                    print("Jammer Stopped")
                    break

            #stopLTE(sockUE, epc, enb)
            #sendRequest('IP3', 'STOP', sockUE)
            time.sleep(5)
    print("End loop")
    sendRequest("CSE", "DONE", connSock)
    file.close()

    sys.exit()
#END handleClient

def handleUE(connSock):
    """Handles a UE over its lifetime"""
    print("Handling UE")

    sendRequest('RUE', 'SRSUE', connSock)

    while True:

        #Get the request from the socket
        data = receive(connSock)

        #Break up the request and parse the URL
        decoded = data
        request = decoded.split('%')

        #print "\nReceived: ", decoded

        #If the client wants my public key
        if("SID" in request[0]):
            print("Identifier Received")

        #If the client wants my public key
        if("IP3" in request[0]):
            print("Reguest for iPerf trial")
            iperfRun('127.0.0.1', connSock)

        #If the the client returned a secure message
        elif("STR" in request[0]):
            if("STARTED" in request[1]):
                print("UE Started")
                time.sleep(10)
                sendRequest("STP", "STOP", connSock)
#END handleClient

def handleClient(connSock):
    """Handles a connection over its lifetime"""

    while True:

        #Get the request from the socket
        data = receive(connSock)

        #Break up the request and parse the URL
        decoded = data
        request = decoded.split('%')

        print ("\nReceived: ", request[0], request[1])

        #If the client wants my public key
        if("SID" in request[0]):
            print("Identifier Received")
            if("JAMMER" in request[1]):
                print("JAMMMER")
                #handleJammer(connSock)
                return "JAMMER"
            elif("SRSUE" in request[1]):
                print("SRSUE")
                #handleUE(connSock)
                return "SRSUE"


#END handleClient


#Set up cmd options
cmdParse = optparse.OptionParser()
cmdParse.add_option("-o", "--outfile", action="store", type="str", dest="outfile", default="out.csv",
                    help="Sets the listening port: Defaults to 2115")
cmdParse.add_option("-v", "--verbose", action="store", type="int", dest="verbose", default=0,
                    help="Prints messages on the wire if set to 1: Defaults to 0")
cmdParse.add_option("-f", "--freq", action="store", type="int", dest="freq", default=2112500,
                    help="Prints messages on the wire if set to 1: Defaults to 2112500")

#Parse the cms and set values
(options, args) = cmdParse.parse_args()
outfile = options.outfile
verbose = options.verbose
freqStart = options.freq

#Setup output outfile
file = open(outfile, "a")

lock = threading.Lock()
ueSock = None
jmSock = None
jF = False
uF = False

#Set up listener
servSock = socket(AF_INET, SOCK_STREAM)
servSock.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)#Reuse the socket
servSock.bind(('10.10.2.4', 2115))#Bind the socket
servSock.listen(1)#Start listening for incoming connections

print ("The server can receive")

while (not jF or not uF):
    print ("Waiting")
    connSock, addr = servSock.accept()
    print ("Accepted new client\n")

    #Begin a new thread for each client
    #x = threading.Thread(target=handleClient, args=(connSock, ), daemon=True)
    #x.start()
    nammed = handleClient(connSock)

    if(nammed == "JAMMER"):
        jmSock = connSock
        jF = True

    elif(nammed == "SRSUE"):
        ueSock = connSock
        uF = True

print ("Have both UE and Jammer")

handleJammer(jmSock, ueSock)
#END main
