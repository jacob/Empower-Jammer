#Import into the namespace
from socket import *
from subprocess import Popen, PIPE, STDOUT
#Import modules
import base64
import os
import sys
import time
import threading
import hashlib
import requests
import optparse
import iperf3
import signal

def startUE():
    print("Starting UE")
    rue = Popen(['srsue', '/etc/srslte/ue.conf'], stdout=PIPE, stdin=PIPE, stderr=PIPE)
    time.sleep(10)
    print("UE Started!")
    return rue
#END startENB

def stopUE(ue):
    print("Stopping UE", ue)
    ue.send_signal(signal.SIGINT)
    time.sleep(10)
    print("UE Stopped")
#END startENB

def sendRequest(method, data, conn):
    """Sends a request to on the given socket"""

    req = method + '%' + data + '%ITS\r\n\r\n'

    if verbose:
        print ("\nSent:\n", req)

    conn.sendall(req.encode())
#END sendRequest

def iperfStart():
    """Run an iperf3 server forever"""
    print ("Starting iperf server")
    return Popen(['iperf3', '-s', '-B', '172.16.0.2'], stdout=PIPE, stdin=PIPE, stderr=PIPE)
    print ("Started server")
#END iperfRun

def iperfStop(Perf):
    """Run an iperf3 server forever"""
    print ("Stopping iperf server")
    Perf.send_signal(signal.SIGINT)
    return
#END iperfRun

def receive(sock):
    """Ensures that the whole response is read from the socket"""

    #Start with an empty response
    response = ""
    print ("Waiting for response")

    #Loop until the everything is received
    while 1:
        data = sock.recv(1024)
        print ("Received response")

        #Add each new chunk to the response
        response = response + data.decode()

        #Catch the end of the header
        if(r'\r\n\r\n' in response or not data or "\r\n\r\n" in response):
            break

    if verbose:
        print ("\nReceived: " + response)

    return response
#END receiveAll

def getPerf(sock):
    """Requests an iPerf run from the host on the socket and waits for a response"""
    #Send a request for a public key
    print ("\n\nAsking for iperf run")
    sendRequest('IP3', 'RuniPerf', sock)

    #Wait for a response
    retval = receive(sock).split("%")#Split the RPK MSG

    print ("Received result")
    print (retval[1])
#END getPerf

def identify(sock):
    """Tells the server what has connected to it"""

    print ("\n\nSending Identity")
    sendRequest('SID', 'SRSUE', sock)
#END identify

def handleServer(connSock):
    """Handles a jammer over its lifetime"""
    print("Waiting For Inst")

    ue = None
    iperf = None

    while True:

        #Get the request from the socket
        data = receive(connSock)

        #Break up the request and parse the URL
        decoded = data
        request = decoded.split('%')

        print ("\nReceived: ", request[0], " ", request[1])

        #If the client wants my public key
        if("RUE" in request[0]):
            ue = startUE()
            sendRequest('STR','STARTED',connSock)

        #If the client wants my public key
        elif("IP3" in request[0]):
            print("Identifier Received")
            if("START" in request[1]):
                print("Starting iPerf")
                iperf = iperfStart()
            elif("STOP" in request[1]):
                print("Stopping iPerf")
                iperfStop(iperf)

        #If the the client returned a secure message
        elif("STP" in request[0]):
            stopUE(ue)
            sendRequest('STR','STOPPED',connSock)
#END handleClient


verbose = 1
host = '10.10.2.4'
port = 2115

try:#Connect to the server
    print ("\nConnecting to:", host, port)
    clientSock = socket(AF_INET, SOCK_STREAM)
    clientSock.connect((host, port))
    print ("Connected")
except Exception:
    print ("Problem with request closed connection")
    exit()

#Begin for the iperf server
#x = threading.Thread(target=iperfRun, args=( ))
#x.start()

identify(clientSock)

handleServer(clientSock)


clientSock.close()
print ("Closed Connection\n\n")
