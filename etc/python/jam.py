#Import into the namespace
from socket import *
from subprocess import Popen, PIPE, STDOUT
#Import modules
import base64
import os
import sys
import time
import threading
import hashlib
import requests
import optparse
import iperf3
import signal

def startJAM(freq):
    print("Starting jammer at ", freq)
    jam = Popen(['python3', 'rfReplay.py', '-f', freq], stdout=PIPE, stdin=PIPE, stderr=PIPE)
    time.sleep(10)
    print("Jammer Started!")
    return jam
#END startENB

def stopJAM(jammer):
    print("Stopping Jammer")
    jammer.communicate("\n".encode())
    time.sleep(10)
    print("Jammer Stopped")
#END startENB

def sendRequest(method, data, conn):
    """Sends a request to on the given socket"""

    req = method + '%' + data + '%ITS\r\n\r\n'

    if verbose:
        print ("\nSent:\n", req)

    conn.sendall(req.encode())
#END sendRequest

def iperfRun():
    """Run an iperf3 server forever"""
    print ("Starting iperf server")
    return Popen(['iperf3', '-s'], stdout=PIPE, stdin=PIPE, stderr=PIPE)

#END sendRequest

def receive(sock):
    """Ensures that the whole response is read from the socket"""

    #Start with an empty response
    response = ""
    print ("Waiting for response")

    #Loop until the everything is received
    while 1:
        data = sock.recv(1024)
        print ("Received response")

        #Add each new chunk to the response
        response = response + data.decode()

        #Catch the end of the header
        if(r'\r\n\r\n' in response or not data or "\r\n\r\n" in response):
            break

    if verbose:
        print ("\nReceived: " + response)

    return response
#END receiveAll

def getPerf(sock):
    """Requests an iPerf run from the host on the socket and waits for a response"""
    #Send a request for a public key
    print ("\n\nAsking for iperf run")
    sendRequest('IP3', 'RuniPerf', sock)

    #Wait for a response
    retval = receive(sock).split("%")#Split the RPK MSG

    print ("Received result")
    print (retval[1])
#END getPerf

def identify(sock):
    """Tells the server what has connected to it"""
    #Send a request for a public key
    print ("\n\nSending Identity")
    sendRequest('SID', 'JAMMER', sock)
#END identify

def handleServer(connSock):
    """Handles a jammer over its lifetime"""
    print("Waiting For Inst")

    jammer = None
    iperfServ = None

    while True:

        #Get the request from the socket
        data = receive(connSock)

        #Break up the request and parse the URL
        decoded = data
        request = decoded.split('%')

        print ("\nReceived: ", request[0], " ", request[1])

        #If the client wants my public key
        if("JAM" in request[0]):
            jammer = startJAM(request[1])
            sendRequest('STR','STARTED',connSock)

        #If the client wants my public key
        elif("IP3" in request[0]):
            print("Reguest for iPerf server")
            iperfServ = iperfRun()

        #If the the client returned a secure message
        elif("STP" in request[0]):
            stopJAM(jammer)
            sendRequest('STR','STOPPED',connSock)

        elif("CSE" in request[0]):
            print ("Alice closed connection\n")
            iperfServ.send_signal(signal.SIGINT)
            return
#END handleClient


verbose = 1
host = '10.10.2.4'
port = 2115

try:#Connect to the server
    print ("\nConnecting to:", host, port)
    clientSock = socket(AF_INET, SOCK_STREAM)
    clientSock.connect((host, port))
    print ("Connected")
except Exception:
    print ("Problem with request closed connection")
    exit()

#Begin for the iperf server
#x = threading.Thread(target=iperfRun, args=( ))
#x.start()

identify(clientSock)

handleServer(clientSock)

clientSock.close()
print ("Closed Connection\n\n")
