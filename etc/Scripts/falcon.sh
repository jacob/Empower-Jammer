#!/bin/bash

echo "Installing Falcon Dependencies"

sudo apt install -y build-essential git subversion cmake libboost-system-dev libboost-test-dev libboost-thread-dev libqwt-dev libqt4-dev libfftw3-dev libsctp-dev libconfig-dev libconfig++-dev libmbedtls-dev
sudo apt install -y libboost-system-dev libboost-test-dev libboost-thread-dev libqwt-qt5-dev qtbase5-dev

git clone https://github.com/srsLTE/srsGUI.git
cd srsGUI
mkdir build
cd build
cmake ../
make
sudo make install

sudo add-apt-repository ppa:ettusresearch/uhd
sudo apt-get update
sudo apt install -y libuhd-dev libuhd003 uhd-host

sudo apt install -y libglib2.0-dev libudev-dev libcurl4-gnutls-dev libboost-all-dev qtdeclarative5-dev libqt5charts5-dev

echo "Starting Falcon Install"
cd

git clone https://github.com/falkenber9/falcon.git
cd falcon
mkdir build
cd build
cmake -DFORCE_SUBPROJECT_SRSLTE=ON ../
make
