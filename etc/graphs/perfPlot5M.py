import matplotlib.pyplot as plt
import numpy as np

def offset(freq):
    return ((2132500 - freq) * .001)

def band(input):
    return (((25*.18)/2) + input/2)

f, m10, m5, wf = np.loadtxt('10To5.csv', delimiter=',', unpack=True)
plt.plot(offset(f),wf, label='WiFi Interference (20MHz Channel)', c='r')
plt.plot(offset(f),m5, label='LTE Interference (5Mhz Channel)', c='g')
plt.plot(offset(f),m10, label='LTE Interference (10Mhz Channel)', c='b')

# x coordinates for the lines
xcoords = [band(16.6), band(25*.18), band(50*.18)]
# colors for the lines
colors = ['r','g','b']
# labels for lines
label = ["WiFi", "5MHz LTE", "10MHz LTE"]

for xc,c,l in zip(xcoords,colors, label):
    plt.axvline(x=xc, label='Direct {} Interference Boundary'.format(l), c=c, linestyle='--')
    plt.axvline(x=-(xc),  c=c,  linestyle='--')

#idx = np.argwhere(np.diff(np.sign(wf - m5))).flatten()
#plt.plot(offset(f[idx]), m10[idx], 'ro')

#idz = np.argwhere(np.diff(np.sign(wf - m10))).flatten()
#plt.plot(offset(f[idz]), m10[idz], 'bo')

plt.xlabel('Offset from center frequency (2132.5MHz)')
plt.ylabel('Bandwidth (Mbps)')
plt.title('Interference Impact\nNUC3(ENB)->NUC4(UE) on 5MHz Channel\nNUC2(Interferer)')
plt.legend()
plt.tight_layout()
plt.show()
