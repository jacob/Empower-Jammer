import matplotlib.pyplot as plt
import numpy as np

def offset(freq):
    return ((2132500 - freq) * .001)

def band(input):
    return (((50*.18)/2) + input/2)

frq, m10, m5, wf = np.loadtxt('out10.csv', delimiter=',', unpack=True)
#plt.plot(x,y, label='Baseline (No Interference)')
plt.plot(offset(frq),wf, label='WiFi Interference (20MHz Channel)', c='r')
plt.plot(offset(frq),m5, label='LTE Interference (5Mhz Channel)', c='g')
plt.plot(offset(frq),m10, label='LTE Interference (10Mhz Channel)', c='b')

# x coordinates for the lines
xcoords = [band(16.6), band(25*.18), band(50*.18)]
# colors for the lines
colors = ['r','g','b']
# labels for lines
label = ["WiFi", "5MHz LTE", "10MHz LTE"]

for xc,c,l in zip(xcoords,colors, label):
    plt.axvline(x=xc, label='Direct {} Interference Boundary'.format(l), c=c, linestyle='--')
    print(xc)
    plt.axvline(x=-(xc),  c=c,  linestyle='--')

plt.xlabel('Offset from center frequency (2132.5MHz)')
plt.ylabel('Bandwidth (Mbps)')
plt.title('Interference Impact\nNUC3(ENB)->NUC4(UE) on 10MHz Channel\nNUC2(Interferer)')
plt.legend()
plt.tight_layout()
plt.show()
